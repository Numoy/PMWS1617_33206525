package de.uniks.pm.game.model;

public class Trainer {
	private String name;
	private String color;
	private int experience;
	private Game game;
	
	//Game methods
	public void setGame(Game game) {
		this.game = game;
	}
	
	public Game getGame() {
		return this.game;
	}
	
	public void withGame() {
		
	}
	
	
	//Classmethods
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getColor() {
		return this.color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public int getExperience() {
		return this.experience;
	}
	public void setExperience(int experience) {
		this.experience = experience;
	}
}
