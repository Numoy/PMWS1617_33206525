package de.uniks.pm.game.model;

public class Dice {
	
	private int value;

	public int getValue() {
		return this.value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
