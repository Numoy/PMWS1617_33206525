package de.uniks.pm.game.model;

import java.util.LinkedHashSet;

public class Ground {
	
	private int x;
	private int y;
	
	private LinkedHashSet<Trainer> trainer;
	private LinkedHashSet<Ground> floor_higher;
	private LinkedHashSet<Ground> floor_lower;
	
	//trainer methods
	public void addTrainer(Trainer trainer) {
		this.trainer.add(trainer);
	}
	
	public void removeTrainer(Trainer trainer) {
		this.trainer.remove(trainer);
	}
	
	public LinkedHashSet<Trainer> getTrainer() {
		return this.trainer;
	}
	
	public void withTrainer() {
		
	}
	
	//Class methods
	public int getX() {
		return this.x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return this.y;
	}
	public void setY(int y) {
		this.y = y;
	}

}
