package de.uniks.pm.game.model;

import java.util.LinkedHashSet;

public class Game {
	
	private int actionPoints;
	
	private LinkedHashSet<Ground> grounds;
	private LinkedHashSet<Trainer> trainer;
	private Dice dice;
	
	
	//Methods Ground
	public void addGround(Ground ground) {
		this.grounds.add(ground);
	}
	
	public void removeGround(Ground ground) {
		this.grounds.remove(grounds);
	}
	
	public LinkedHashSet<Ground> getGround() {
		return this.grounds;
	}
	
	public void withGround() {
		
	}
	
	
	//Methods Trainer
	public void addTrainer(Trainer trainer) {
		this.trainer.add(trainer);
	}
	
	public void removeTrainer(Trainer trainer) {
		this.trainer.remove(trainer);
	}
	
	public LinkedHashSet<Trainer> getTrainer() {
		return this.trainer;
	}
	
	public void withTrainer() {
		
	}
	
	//Methods Dice
	public void setDice(Dice dice) {
		this.dice = dice;
	}
	
	public Dice getDice() {
		return this.dice;
	}
	
	public void withDice() {
		
	}
	
	//Classmethods
	public void setActionPoints(int actionPoints) {
		this.actionPoints = actionPoints;
	}
	
	public int getActionPoints() {
		return this.actionPoints;
	}
	
	public boolean checkEnd() {
		return true;
	}

}
